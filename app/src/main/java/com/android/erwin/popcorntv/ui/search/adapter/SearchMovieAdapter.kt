package com.android.erwin.popcorntv.ui.search.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.android.erwin.popcorntv.databinding.ItemMovie2Binding
import com.android.erwin.popcorntv.model.Movie

class SearchMovieAdapter(
    private val onMovieClick: (movie: Movie) -> Unit
) : PagingDataAdapter<Movie, SearchMovieAdapter.MovieViewHolder>(DiffUtilCallBack()) {

    inner class MovieViewHolder(val item : ItemMovie2Binding): RecyclerView.ViewHolder(item.root)

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        getItem(position)?.let { movie ->
            holder.item.movie = movie
            holder.itemView.setOnClickListener { onMovieClick.invoke(movie) }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MovieViewHolder {
        return MovieViewHolder(
            ItemMovie2Binding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    class DiffUtilCallBack: DiffUtil.ItemCallback<Movie>() {
        override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
            return oldItem.posterPath == newItem.posterPath
        }

        override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
            return oldItem == newItem
        }
    }
}