package com.android.erwin.popcorntv.ui.detail_movie

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.erwin.popcorntv.model.DetailMovieResponse
import com.android.erwin.popcorntv.model.ReviewResponse
import com.android.erwin.popcorntv.model.TrailerResponse
import com.android.erwin.popcorntv.remote.API
import com.android.erwin.popcorntv.remote.Retrofit
import com.android.erwin.popcorntv.utils.Tools
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailMovieViewModel : ViewModel() {
    var liveDataMovie: MutableLiveData<DetailMovieResponse?> = MutableLiveData()
    var liveDataTrailer: MutableLiveData<TrailerResponse.GetResponseTrailer?> = MutableLiveData()
    var liveDataReviewResponse: MutableLiveData<ReviewResponse.GetReviewResponse?> = MutableLiveData()

    fun getLiveDataDetailMovie(): MutableLiveData<DetailMovieResponse?> {
        return liveDataMovie
    }

    fun getLiveDataTrailerLink(): MutableLiveData<TrailerResponse.GetResponseTrailer?> {
        return liveDataTrailer
    }

    fun getLiveDataReviewMovie(): MutableLiveData<ReviewResponse.GetReviewResponse?> {
        return liveDataReviewResponse
    }

    fun getApiDetailMovie(
        movie_id: String,
    ) {
        val retroInstance = Retrofit.getRetrofitMovieInstance()
        val retroService = retroInstance.create(API::class.java)
        val call = retroService.getDetailMovie(
            movie_id,
            Tools.API_KEY_MOVIE,
        )
        call.enqueue(object : Callback<DetailMovieResponse> {
            override fun onFailure(call: Call<DetailMovieResponse>, t: Throwable) {
                liveDataMovie.postValue(null)
            }

            override fun onResponse(
                call: Call<DetailMovieResponse>,
                response: Response<DetailMovieResponse>
            ) {
                if (response.isSuccessful) {
                    liveDataMovie.postValue(response.body())
                } else {
                    liveDataMovie.postValue(null)
                }
            }
        })
    }

    fun getApiTrailerLink(
        movie_id: String
    ) {
        val retroInstance = Retrofit.getRetrofitMovieInstance()
        val retroService = retroInstance.create(API::class.java)
        val call = retroService.getTrailerLink(
            movie_id,
            Tools.API_KEY_MOVIE,
        )
        call.enqueue(object : Callback<TrailerResponse.GetResponseTrailer> {
            override fun onFailure(call: Call<TrailerResponse.GetResponseTrailer>, t: Throwable) {
                liveDataTrailer.postValue(null)
            }

            override fun onResponse(
                call: Call<TrailerResponse.GetResponseTrailer>,
                response: Response<TrailerResponse.GetResponseTrailer>
            ) {
                if (response.isSuccessful) {
                    liveDataTrailer.postValue(response.body())
                } else {
                    liveDataTrailer.postValue(null)
                }
            }
        })
    }

    fun getApiReviewMovie(
        movie_id: String,
        page: Int,
    ) {
        val retroInstance = Retrofit.getRetrofitMovieInstance()
        val retroService = retroInstance.create(API::class.java)
        val call = retroService.getReviewMovie(
            movie_id,
            Tools.API_KEY_MOVIE,
            page,
        )
        call.enqueue(object : Callback<ReviewResponse.GetReviewResponse> {
            override fun onFailure(call: Call<ReviewResponse.GetReviewResponse>, t: Throwable) {
                liveDataReviewResponse.postValue(null)
            }

            override fun onResponse(
                call: Call<ReviewResponse.GetReviewResponse>,
                response: Response<ReviewResponse.GetReviewResponse>
            ) {
                if (response.isSuccessful) {
                    liveDataReviewResponse.postValue(response.body())
                } else {
                    liveDataReviewResponse.postValue(null)
                }
            }
        })
    }
}