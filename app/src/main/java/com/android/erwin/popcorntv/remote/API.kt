package com.android.erwin.popcorntv.remote

import com.android.erwin.popcorntv.model.*
import com.android.erwin.popcorntv.utils.Tools
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.util.concurrent.TimeUnit

interface API {
    @GET("movie/popular")
    fun getPopularMovie(
        @Query("api_key") api_key: String,
        @Query("page") page: Int,
    ): Call<MovieResponse.GetResponseMovie>

    @GET("movie/top_rated")
    fun getTopRatedMovie(
        @Query("api_key") api_key: String,
        @Query("page") page: Int,
    ): Call<MovieResponse.GetResponseMovie>

    @GET("movie/upcoming")
    fun getUpcomingMovie(
        @Query("api_key") api_key: String,
        @Query("page") page: Int,
    ): Call<MovieResponse.GetResponseMovie>

    @GET("genre/movie/list")
    fun getGenreMovie(
        @Query("api_key") api_key: String,
    ): Call<GenreResponse.GetResponseGenre>

    @GET("discover/movie")
    fun getMovieByGenre(
        @Query("api_key") api_key: String,
        @Query("page") page: Int,
        @Query("with_genres") genre_id: String,
    ): Call<MovieResponse.GetResponseMovie>

    @GET("movie/{movie_id}")
    fun getDetailMovie(
        @Path("movie_id") movie_id: String,
        @Query("api_key") api_key: String,
    ): Call<DetailMovieResponse>

    @GET("movie/{movie_id}/videos")
    fun getTrailerLink(
        @Path("movie_id") movie_id: String,
        @Query("api_key") api_key: String,
    ): Call<TrailerResponse.GetResponseTrailer>

    @GET("movie/{movie_id}/reviews")
    fun getReviewMovie(
        @Path("movie_id") movie_id: String,
        @Query("api_key") api_key: String,
        @Query("page") page: Int,
    ): Call<ReviewResponse.GetReviewResponse>

    @GET("search/movie")
    suspend fun getSearchMovie(
        @Query("api_key") api_key: String,
        @Query("page") page: Int,
        @Query("query") query: String,
    ): Response<com.android.erwin.popcorntv.model.response.MovieResponse>

    companion object {
        operator fun invoke() : API {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(logging)
                .readTimeout(90, TimeUnit.SECONDS)
                .connectTimeout(90, TimeUnit.SECONDS)
                .build()

            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(Tools.BASE_URL_MOVIE)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(API::class.java)
        }
    }
}