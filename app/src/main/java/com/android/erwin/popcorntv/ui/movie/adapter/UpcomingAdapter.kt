package com.android.erwin.popcorntv.ui.movie.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.android.erwin.popcorntv.model.MovieResponse
import com.android.erwin.popcorntv.databinding.ItemMovieBinding
import com.android.erwin.popcorntv.databinding.ItemUpcomingBinding
import com.android.erwin.popcorntv.utils.Tools

class UpcomingAdapter(
    private var movieResponses: MutableList<MovieResponse>,
    private val onMovieClick: (movieResponse: MovieResponse) -> Unit
) : RecyclerView.Adapter<UpcomingAdapter.MovieViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val view = LayoutInflater.from(parent.context)
        val binding = ItemUpcomingBinding.inflate(view, parent, false)
        return MovieViewHolder(binding)
    }

    override fun getItemCount(): Int = movieResponses.size

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(movieResponses[position])
    }

    fun appendMovies(movieResponses: List<MovieResponse>) {
        this.movieResponses.addAll(movieResponses)
        notifyItemRangeInserted(
            this.movieResponses.size,
            movieResponses.size - 1
        )
        notifyDataSetChanged()
    }

    inner class MovieViewHolder(itemView: ItemUpcomingBinding) : RecyclerView.ViewHolder(itemView.root) {
        private val binding = itemView

        fun bind(movieResponse: MovieResponse) {

            binding.tvTitle.text = movieResponse.title
            binding.tvDate.text = movieResponse.releaseDate

            Glide.with(itemView.context)
                .load(Tools.BASE_PATH_POSTER + movieResponse.posterPath)
                .transform(CenterCrop())
                .into(binding.imgBanner)

            itemView.setOnClickListener { onMovieClick.invoke(movieResponse) }
        }
    }
}