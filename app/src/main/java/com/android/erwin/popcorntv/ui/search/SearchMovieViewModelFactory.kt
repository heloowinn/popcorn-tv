package com.android.erwin.popcorntv.ui.search

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.android.erwin.popcorntv.remote.API


class SearchMovieViewModelFactory(private val api: API) : ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return SearchMovieViewModel(api) as T
    }
}