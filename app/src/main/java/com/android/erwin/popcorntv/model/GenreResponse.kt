package com.android.erwin.popcorntv.model

import com.google.gson.annotations.SerializedName

data class GenreResponse(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
) {
    data class GetResponseGenre(
        @SerializedName("genres") val genreResponses: List<GenreResponse>
    )
}