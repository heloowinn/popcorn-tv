package com.android.erwin.popcorntv.ui.genre

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.erwin.popcorntv.databinding.FragmentGenreBinding
import com.android.erwin.popcorntv.model.GenreResponse
import com.android.erwin.popcorntv.model.MovieResponse
import com.android.erwin.popcorntv.ui.detail_movie.DetailMovieActivity
import com.android.erwin.popcorntv.ui.filter_genre.GenreMovieActivity
import com.android.erwin.popcorntv.ui.movie.MovieFragment
import com.android.erwin.popcorntv.ui.movie.MovieViewModel
import com.android.erwin.popcorntv.ui.genre.adapter.GenreAdapter
import com.android.erwin.popcorntv.utils.Tools

class GenreFragment : Fragment() {

    companion object {
        fun newInstance() = MovieFragment()
    }

    private lateinit var binding: FragmentGenreBinding
    private lateinit var viewModel: MovieViewModel

    private lateinit var genreAdapter: GenreAdapter
    private lateinit var genreLayoutMgr: LinearLayoutManager
    private lateinit var moviesLayoutMgr: GridLayoutManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentGenreBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MovieViewModel::class.java)


        // genre

        moviesLayoutMgr = GridLayoutManager(
            context,4
        )

        genreLayoutMgr = LinearLayoutManager(
            context,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        binding.genreList.layoutManager = moviesLayoutMgr
        genreAdapter = GenreAdapter(mutableListOf()) { genre -> showMovieByGenre(genre) }
        binding.genreList.adapter = genreAdapter

        getGenreMovies()
    }

    private fun getGenreMovies() {
        viewModel.getLiveDataGenre().observe(viewLifecycleOwner) {
            if (it != null) {
                genreAdapter.appendGenres(it.genreResponses)
            } else {
                Toast.makeText(context, "Error get data genre", Toast.LENGTH_SHORT).show()
            }
        }
        viewModel.getApiGenreList()
    }


    private fun showMovieDetails(movieResponse: MovieResponse) {
        val intent = Intent(context, DetailMovieActivity::class.java)
        intent.putExtra(Tools.MOVIE_ID, movieResponse.id)
        startActivity(intent)
    }

    private fun showMovieByGenre(genreResponse: GenreResponse) {
        val intent = Intent(context, GenreMovieActivity::class.java)
        intent.putExtra(Tools.GENRE_ID, genreResponse.id)
        intent.putExtra(Tools.GENRE_NAME, genreResponse.name)
        startActivity(intent)
    }

}