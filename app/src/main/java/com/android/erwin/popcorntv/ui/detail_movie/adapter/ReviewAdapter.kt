package com.android.erwin.popcorntv.ui.detail_movie.adapter

import android.graphics.drawable.Drawable
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.erwin.popcorntv.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.android.erwin.popcorntv.model.ReviewResponse
import com.android.erwin.popcorntv.databinding.ItemReviewBinding
import com.android.erwin.popcorntv.utils.Tools
import java.lang.Exception
import java.lang.StringBuilder
import java.text.SimpleDateFormat

class ReviewAdapter(
    private var reviewResponses: MutableList<ReviewResponse>,
    private var onReviewClick: (reviewResponse: ReviewResponse) -> Unit
) : RecyclerView.Adapter<ReviewAdapter.ReviewViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewViewHolder {
        val view = LayoutInflater.from(parent.context)
        val binding = ItemReviewBinding.inflate(view, parent, false)
        return ReviewViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ReviewViewHolder, position: Int) {
        holder.bind(reviewResponses[position])
    }

    override fun getItemCount(): Int = reviewResponses.size

    fun appendReviews(reviewResponses: List<ReviewResponse>) {
        this.reviewResponses.addAll(reviewResponses)
        notifyItemRangeInserted(
            this.reviewResponses.size,
            reviewResponses.size - 1
        )
        notifyDataSetChanged()
    }

    inner class ReviewViewHolder(itemView: ItemReviewBinding) : RecyclerView.ViewHolder(itemView.root) {
        private val binding = itemView

        fun bind(reviewResponse: ReviewResponse) {
            if (reviewResponse.author_details.photo != null) {
                val url = StringBuilder(reviewResponse.author_details.photo)
                if (url.indexOf("http") == -1) {
                    url.insert(0, Tools.BASE_PATH_AVATAR)
                } else {
                    url.deleteCharAt(0)
                }
                Glide.with(itemView.context)
                    .load(url.toString())
                    .transform(CenterCrop())
                    .addListener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            binding.profileImage.setImageResource(R.drawable.ic_account)
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            return false
                        }
                    })
                    .into(binding.profileImage)
            }

            binding.author.text = reviewResponse.author
            try {
                val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                val formatter = SimpleDateFormat("dd MMMM yyyy")
                val dt = formatter.format(parser.parse(reviewResponse.updated_at))
                binding.date.text = dt
            } catch (e: Exception) {
                binding.date.text = reviewResponse.updated_at
            }
            if (reviewResponse.author_details.rating != null) {
                binding.reviewRate.text = reviewResponse.author_details.rating.toString()
            } else {
                binding.reviewRate.text = "0.0"
            }
            binding.content.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(reviewResponse.content, Html.FROM_HTML_MODE_COMPACT)
            } else {
                Html.fromHtml(reviewResponse.content)
            }

            itemView.setOnClickListener { onReviewClick.invoke(reviewResponse) }
        }
    }
}