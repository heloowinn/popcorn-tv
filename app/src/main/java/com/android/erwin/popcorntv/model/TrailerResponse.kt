package com.android.erwin.popcorntv.model

import com.google.gson.annotations.SerializedName

data class TrailerResponse(
    @SerializedName("name") val title: String,
    @SerializedName("key") val key: String,
    @SerializedName("site") val site: String,
) {
    data class GetResponseTrailer(
        @SerializedName("results") val results: List<TrailerResponse>
    )
}
