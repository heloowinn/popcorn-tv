package com.android.erwin.popcorntv.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.android.erwin.popcorntv.model.Movie
import com.android.erwin.popcorntv.remote.API
import com.android.erwin.popcorntv.utils.Tools
import okio.IOException
import retrofit2.HttpException

class SearchMoviePagingSource(private val api: API, private val keyword: String) : PagingSource<Int, Movie>() {
    override val keyReuseSupported: Boolean = true

    override fun getRefreshKey(state: PagingState<Int, Movie>): Int? {
        return state.anchorPosition
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Movie> {
        return try {
            val page: Int = params.key ?: FIRST_PAGE_INDEX
            val response = api.getSearchMovie(
                api_key = Tools.API_KEY_MOVIE,
                page = page,
                query = keyword
            )
            if (response.isSuccessful) {
                LoadResult.Page(
                    data = response.body()!!.results,
                    prevKey = if (page == FIRST_PAGE_INDEX) null else page - 1,
                    nextKey = if (response.body()?.total_pages!! <= page) null else page + 1
                )
            } else {
                throw IOException("Failed fetch data")
            }
        } catch (e: HttpException) {
            return LoadResult.Error(e)
        } catch (e: IOException) {
            return LoadResult.Error(e)
        }
    }

    companion object {
        private const val FIRST_PAGE_INDEX = 1
    }
}