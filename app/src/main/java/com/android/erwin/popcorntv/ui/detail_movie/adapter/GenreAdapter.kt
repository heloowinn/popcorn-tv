package com.android.erwin.popcorntv.ui.detail_movie.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.android.erwin.popcorntv.R
import com.android.erwin.popcorntv.model.GenreResponse
import com.android.erwin.popcorntv.databinding.ItemGenreBinding

class GenreAdapter(
    private var genreResponses: MutableList<GenreResponse>,
    private val onGenreClick: (genreResponse: GenreResponse) -> Unit
) : RecyclerView.Adapter<GenreAdapter.GenreViewHolder>() {

    inner class GenreViewHolder(val item: ItemGenreBinding) : RecyclerView.ViewHolder(item.root)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): GenreViewHolder {
        return GenreViewHolder(DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_genre,
            parent,
            false
        ))
    }

    override fun onBindViewHolder(holder: GenreViewHolder, position: Int) {
        val genre = genreResponses[position]
        if (genre.name.equals("Action")){
            holder.item.imgGenre.setImageResource(R.drawable.ic_action)
        }else if (genre.name.equals("Adventure")) {
            holder.item.imgGenre.setImageResource(R.drawable.ic_adventurer)
        }else if (genre.name.equals("Animation")) {
            holder.item.imgGenre.setImageResource(R.drawable.ic_animation)
        }else if (genre.name.equals("Comedy")) {
            holder.item.imgGenre.setImageResource(R.drawable.ic_comedy)
        }else if (genre.name.equals("Crime")) {
            holder.item.imgGenre.setImageResource(R.drawable.ic_crime)
        }else if (genre.name.equals("Documentary")) {
            holder.item.imgGenre.setImageResource(R.drawable.ic_documentary)
        }else if (genre.name.equals("Drama")) {
            holder.item.imgGenre.setImageResource(R.drawable.ic_theatre)
        }else if (genre.name.equals("Family")) {
            holder.item.imgGenre.setImageResource(R.drawable.ic_family)
        }else if (genre.name.equals("Fantasy")) {
            holder.item.imgGenre.setImageResource(R.drawable.ic_fantasy)
        }else if (genre.name.equals("History")) {
            holder.item.imgGenre.setImageResource(R.drawable.ic_history)
        }else if (genre.name.equals("Horror")) {
            holder.item.imgGenre.setImageResource(R.drawable.ic_horror)
        }else if (genre.name.equals("Music")) {
            holder.item.imgGenre.setImageResource(R.drawable.ic_music)
        }else if (genre.name.equals("Mystery")) {
            holder.item.imgGenre.setImageResource(R.drawable.ic_mystery)
        }else if (genre.name.equals("Romance")) {
            holder.item.imgGenre.setImageResource(R.drawable.ic_romance)
        }else if (genre.name.equals("Science Fiction")) {
            holder.item.imgGenre.setImageResource(R.drawable.ic_science_fiction)
        }else if (genre.name.equals("TV Movie")) {
            holder.item.imgGenre.setImageResource(R.drawable.ic_cinema)
        }else if (genre.name.equals("Thriller")) {
            holder.item.imgGenre.setImageResource(R.drawable.ic_thriller)
        }else if (genre.name.equals("War")) {
            holder.item.imgGenre.setImageResource(R.drawable.ic_war)
        }else if (genre.name.equals("Western")) {
            holder.item.imgGenre.setImageResource(R.drawable.ic_western)
        }else{
            holder.item.imgGenre.setImageResource(R.mipmap.ic_launcher_round)
        }
        holder.item.genre = genre
        holder.itemView.setOnClickListener { onGenreClick.invoke(genre) }
        holder.item.genre = genre
        holder.itemView.setOnClickListener { onGenreClick.invoke(genre) }
    }

    override fun getItemCount(): Int = genreResponses.size

    fun appendGenres(genreResponses: List<GenreResponse>) {
        this.genreResponses.addAll(genreResponses)
        notifyItemRangeInserted(
            this.genreResponses.size,
            genreResponses.size - 1
        )
        notifyDataSetChanged()
    }

}