package com.android.erwin.popcorntv.ui.filter_genre

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.erwin.popcorntv.model.MovieResponse
import com.android.erwin.popcorntv.remote.API
import com.android.erwin.popcorntv.remote.Retrofit
import com.android.erwin.popcorntv.utils.Tools
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GenreMovieViewModel : ViewModel() {
    lateinit var liveDataListMovieResponse: MutableLiveData<MovieResponse.GetResponseMovie?>

    init {
        liveDataListMovieResponse = MutableLiveData()
    }

    fun getLiveDataMovie(): MutableLiveData<MovieResponse.GetResponseMovie?> {
        return liveDataListMovieResponse
    }

    fun getApiMoviesByGenre(
        genre_id: String,
        page: Int = 1
    ) {
        val retroInstance = Retrofit.getRetrofitMovieInstance()
        val retroService = retroInstance.create(API::class.java)
        val call = retroService.getMovieByGenre(
            Tools.API_KEY_MOVIE,
            page,
            genre_id
        )
        call.enqueue(object : Callback<MovieResponse.GetResponseMovie> {
            override fun onFailure(call: Call<MovieResponse.GetResponseMovie>, t: Throwable) {
                liveDataListMovieResponse.postValue(null)
            }

            override fun onResponse(
                call: Call<MovieResponse.GetResponseMovie>,
                response: Response<MovieResponse.GetResponseMovie>
            ) {
                if (response.isSuccessful) {
                    liveDataListMovieResponse.postValue(response.body())
                } else {
                    liveDataListMovieResponse.postValue(null)
                }
            }
        })
    }
}