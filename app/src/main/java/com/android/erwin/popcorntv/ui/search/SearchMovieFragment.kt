package com.android.erwin.popcorntv.ui.search

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.android.erwin.popcorntv.databinding.FragmentSearchMovieBinding
import com.android.erwin.popcorntv.model.Movie
import com.android.erwin.popcorntv.remote.API
import com.android.erwin.popcorntv.ui.detail_movie.DetailMovieActivity
import com.android.erwin.popcorntv.utils.PagingLoadStateAdapter
import com.android.erwin.popcorntv.utils.SpacingItemDecoration
import com.android.erwin.popcorntv.utils.Tools
import com.android.erwin.popcorntv.utils.Tools.Companion.hideKeyboard
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import com.android.erwin.popcorntv.ui.search.adapter.SearchMovieAdapter

class SearchMovieFragment : Fragment() {

    companion object {
        fun newInstance() = SearchMovieFragment()
    }

    private lateinit var viewModel: SearchMovieViewModel
    private lateinit var binding: FragmentSearchMovieBinding

    private lateinit var mAdapter: SearchMovieAdapter
    private lateinit var moviesLayoutMgr: StaggeredGridLayoutManager

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val api = API()
        val factory = SearchMovieViewModelFactory(api = api)
        viewModel = ViewModelProvider(this, factory)[SearchMovieViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSearchMovieBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        moviesLayoutMgr = StaggeredGridLayoutManager(
            Tools.getGridSpanCountMovie(requireActivity()),
            StaggeredGridLayoutManager.VERTICAL
        )
        mAdapter = SearchMovieAdapter { movie -> showDetailMovie(movie) }
        binding.apply {
            recyclerViewSearch.layoutManager = StaggeredGridLayoutManager(Tools.getGridSpanCountMovie(requireActivity()), StaggeredGridLayoutManager.VERTICAL)
            val decoration = SpacingItemDecoration(Tools.getGridSpanCountMovie(requireActivity()), Tools.dpToPx(requireContext(), 2), false)
            recyclerViewSearch.addItemDecoration(decoration)
            mAdapter.apply {
                recyclerViewSearch.adapter = withLoadStateHeaderAndFooter(
                    header = PagingLoadStateAdapter(this),
                    footer = PagingLoadStateAdapter(this)
                )
                addLoadStateListener { loadState ->
                    if (loadState.refresh is LoadState.Loading) {
                        isLoadingMovie(true)
                    } else if (loadState.refresh is LoadState.NotLoading) {
                        isLoadingMovie(false)
                    }
                    if (loadState.refresh is LoadState.NotLoading &&
                        loadState.append.endOfPaginationReached &&
                        mAdapter.itemCount < 1) {
                        binding.movieNotFound.visibility = View.VISIBLE
                    }
                }
            }


            edtSearch.setOnEditorActionListener { _, id, _ ->
                if (id == EditorInfo.IME_ACTION_SEARCH) {
                    searchMovie(edtSearch.text.toString().trim())
                }
                false
            }
        }
    }

    private fun isLoadingMovie(bool: Boolean) {
        binding.movieNotFound.visibility = View.GONE
        binding.shimmerSearchMovie.visibility = if (bool) View.VISIBLE else View.GONE
        binding.recyclerViewSearch.visibility = if (bool) View.GONE else View.VISIBLE
    }

    private fun searchMovie(keyword: String) {
        view?.let { context?.hideKeyboard(it) }
        lifecycleScope.launch {
            viewModel.getSearchMovie(keyword).collectLatest {
                mAdapter.submitData(it)
            }
        }
    }

    private fun showDetailMovie(movie: Movie) {
        val intent = Intent(context, DetailMovieActivity::class.java)
        intent.putExtra(Tools.MOVIE_ID, movie.id)
        startActivity(intent)
    }

}