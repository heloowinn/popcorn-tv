package com.android.erwin.popcorntv.ui.movie

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.erwin.popcorntv.model.GenreResponse
import com.android.erwin.popcorntv.model.MovieResponse
import com.android.erwin.popcorntv.remote.API
import com.android.erwin.popcorntv.remote.Retrofit
import com.android.erwin.popcorntv.utils.Tools
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieViewModel : ViewModel() {
    var liveDataListPopular: MutableLiveData<MovieResponse.GetResponseMovie?>
    var liveDataListTopRated: MutableLiveData<MovieResponse.GetResponseMovie?>
    var liveDataListUpcoming: MutableLiveData<MovieResponse.GetResponseMovie?>
    var liveDataListGenreResponse: MutableLiveData<GenreResponse.GetResponseGenre?>

    init {
        liveDataListPopular = MutableLiveData()
        liveDataListTopRated = MutableLiveData()
        liveDataListUpcoming = MutableLiveData()
        liveDataListGenreResponse = MutableLiveData()
    }

    fun getLiveDataPopular(): MutableLiveData<MovieResponse.GetResponseMovie?> {
        return liveDataListPopular
    }

    fun getLiveDataTopRated(): MutableLiveData<MovieResponse.GetResponseMovie?> {
        return liveDataListTopRated
    }

    fun getLiveDataUpcoming(): MutableLiveData<MovieResponse.GetResponseMovie?> {
        return liveDataListUpcoming
    }

    fun getLiveDataGenre(): MutableLiveData<GenreResponse.GetResponseGenre?> {
        return liveDataListGenreResponse
    }

    fun getApiMoviesPopular(
        page: Int = 1
    ) {
        val retroInstance = Retrofit.getRetrofitMovieInstance()
        val retroService = retroInstance.create(API::class.java)
        val call = retroService.getPopularMovie(
            Tools.API_KEY_MOVIE,
            page
        )
        call.enqueue(object : Callback<MovieResponse.GetResponseMovie> {
            override fun onFailure(call: Call<MovieResponse.GetResponseMovie>, t: Throwable) {
                liveDataListPopular.postValue(null)
            }

            override fun onResponse(
                call: Call<MovieResponse.GetResponseMovie>,
                response: Response<MovieResponse.GetResponseMovie>
            ) {
                if (response.isSuccessful) {
                    liveDataListPopular.postValue(response.body())
                } else {
                    liveDataListPopular.postValue(null)
                }
            }
        })
    }

    fun getApiMoviesTopRated(
        page: Int = 1
    ) {
        val retroInstance = Retrofit.getRetrofitMovieInstance()
        val retroService = retroInstance.create(API::class.java)
        val call = retroService.getTopRatedMovie(
            Tools.API_KEY_MOVIE,
            page
        )
        call.enqueue(object : Callback<MovieResponse.GetResponseMovie> {
            override fun onFailure(call: Call<MovieResponse.GetResponseMovie>, t: Throwable) {
                liveDataListTopRated.postValue(null)
            }

            override fun onResponse(
                call: Call<MovieResponse.GetResponseMovie>,
                response: Response<MovieResponse.GetResponseMovie>
            ) {
                if (response.isSuccessful) {
                    liveDataListTopRated.postValue(response.body())
                } else {
                    liveDataListTopRated.postValue(null)
                }
            }
        })
    }

    fun getApiMoviesUpcoming(
        page: Int = 1
    ) {
        val retroInstance = Retrofit.getRetrofitMovieInstance()
        val retroService = retroInstance.create(API::class.java)
        val call = retroService.getUpcomingMovie(
            Tools.API_KEY_MOVIE,
            page
        )
        call.enqueue(object : Callback<MovieResponse.GetResponseMovie> {
            override fun onFailure(call: Call<MovieResponse.GetResponseMovie>, t: Throwable) {
                liveDataListUpcoming.postValue(null)
            }

            override fun onResponse(
                call: Call<MovieResponse.GetResponseMovie>,
                response: Response<MovieResponse.GetResponseMovie>
            ) {
                if (response.isSuccessful) {
                    liveDataListUpcoming.postValue(response.body())
                } else {
                    liveDataListUpcoming.postValue(null)
                }
            }
        })
    }

    fun getApiGenreList() {
        val retroInstance = Retrofit.getRetrofitMovieInstance()
        val retroService = retroInstance.create(API::class.java)
        val call = retroService.getGenreMovie(
            Tools.API_KEY_MOVIE
        )
        call.enqueue(object : Callback<GenreResponse.GetResponseGenre> {
            override fun onResponse(
                call: Call<GenreResponse.GetResponseGenre>,
                response: Response<GenreResponse.GetResponseGenre>
            ) {
                if (response.isSuccessful) {
                    liveDataListGenreResponse.postValue(response.body())
                } else {
                    liveDataListGenreResponse.postValue(null)
                }
            }

            override fun onFailure(call: Call<GenreResponse.GetResponseGenre>, t: Throwable) {
                liveDataListGenreResponse.postValue(null)
            }
        })
    }
}