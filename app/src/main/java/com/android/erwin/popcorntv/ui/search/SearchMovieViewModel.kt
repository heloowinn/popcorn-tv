package com.android.erwin.popcorntv.ui.search

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.android.erwin.popcorntv.model.Movie
import com.android.erwin.popcorntv.paging.SearchMoviePagingSource
import com.android.erwin.popcorntv.remote.API
import kotlinx.coroutines.flow.Flow


class SearchMovieViewModel(private val api: API) : ViewModel() {

    fun getSearchMovie(keyword: String): Flow<PagingData<Movie>> {
        return Pager (config = PagingConfig(pageSize = 20, maxSize = 200),
            pagingSourceFactory = { SearchMoviePagingSource(api, keyword) }).flow.cachedIn(viewModelScope)
    }

}