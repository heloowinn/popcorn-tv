package com.android.erwin.popcorntv.model

import com.google.gson.annotations.SerializedName

data class SearchResponse(
    @SerializedName("id") val id: Int,
    @SerializedName("poster_path") val posterPath: String,
    @SerializedName("original_title") val title: String,
    @SerializedName("backdrop_path") val backdropPath: String,
){
    data class GetResponseSearch(
        @SerializedName("results") val results: List<SearchResponse>
    )
}